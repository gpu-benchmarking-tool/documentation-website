$(document).ready(function () {
    $('#img1')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Tool-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Tool.svg");
    });

    $('#img2')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/GL-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/GL.svg");
    });

    $('#img3')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Vulkan-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Vulkan.svg");
    });

    $('#img4')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Profiler-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Profiler.svg");
    });

    $('#img5')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/ObjLoader-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/ObjLoader.svg");
    });

    $('#img6')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Triangulate-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Triangulate.svg");
    });

    $('#img7')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/MathLib-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/MathLib.svg");
    });

    $('#img8')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Assets-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Assets.svg");
    });

    $('#img9')
        .mouseover(function () {
        $(this).attr("src", "assets/Icons/Doc-hover.svg");
    })
        .mouseout(function () {
        $(this).attr("src", "assets/Icons/Doc.svg");
    });
});