# Documentation Website

*This repository is part of the [**GPU Benchmarking Tool**](https://gpu-benchmarking-documentation.firebaseapp.com/) project.*  

This is a website used to host the documentation and links to the different components of the project.

## Compile and run (Windows, Linux and macOS)
- Make sure you have git installed
    - [Git for Windows](https://git-scm.com/download/win)
    - [Git for Linux / macOS](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Clone the repository `git clone https://gitlab.com/gpu-benchmarking-tool/documentation-website.git`
- Enter the cloned directory and clone the submodule `git submodule update --init`
- Finally, open index.html in any browser